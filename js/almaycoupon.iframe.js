(function () {
    var global = this,
        window = global.window,
        document = global.document,

        Trapeze = global.Trapeze = (global.Trapeze || { }),
        IFrame = Trapeze.IFrame  = (Trapeze.IFrame || {});

    IFrame.initialize = function () {
        if (window.self === window.top) {
            var body = document.getElementsByTagName('body')[0];
            var html = document.getElementsByTagName('html')[0];
            body.style.overflow = 'visible';
            html.style.overflow = 'visible';
        }
    };

    IFrame.isIFrame = function () {
        return window.self !== window.top;
    };

}).call(this);
