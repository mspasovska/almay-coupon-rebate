(function () {
    var global = this,
        _ = global._,
        $ = global.$,

        Trapeze = (global.Trapeze || (global.Trapeze = { })),

        AlmayCoupon = Trapeze.AlmayCoupon = Trapeze.AlmayCoupon || { },

        Facebook = AlmayCoupon.Facebook = {},

        callbacks = {
            'initialized': []
        },

        FBCanvasAPI = null;

    Facebook.initialize = function (FB) {
        FB.Canvas.scrollTo(0,0);
        FB.Canvas.setSize({
            height: 800
        });

        _.delay(function () {
            FB.Canvas.setAutoGrow();
            _(callbacks.initialized).each(function (callback) {
                callback(new FBCanvasAPI(FB));
            });
        }, 200);
    };

    Facebook.on = function (type, callback) {
        if (callbacks[type]) {
            callbacks[type].push(callback);
        }
    };

    /*-----------------------------------------------*/

    FBCanvasAPI = function (FB) {

        this.FB = FB;
        this.interval = 10;
    };

    FBCanvasAPI.prototype.scrollTo = function (y, duration) {
        this.FB.Canvas.getPageInfo(_(function(pageInfo) {
            $({y: pageInfo.scrollTop}).animate(
                {y: y},
                {
                    duration: duration,
                    step: _(function(offset) {
                        this.FB.Canvas.scrollTo(0, offset);
                    }).bind(this)
                }
            );
        }).bind(this));
    };

    FBCanvasAPI.prototype.post = function (args) {

        var params = {
                method: 'feed',
                link: args.link,
                picture: args.imageUrl,
                name: args.title,
                caption: args.caption,
                description: args.description
            },
            deferred = $.Deferred();

        this.FB.ui(params, function(response) {
            if (response && response.post_id) {
                deferred.resolve(response);
            } else {
                deferred.reject(response);
            }
        });

        return deferred.promise();
    };

}).call(this);
