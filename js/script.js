/*
 * Almay Coupon Rebate
 *
 *
 * Copyright (c) 2014 Trapeze
  */

(function () {
    var global = this;
    var NAMESPACE = 'AlmayCoupon-Rebate';


    // Initial setup
    // =============

    // Map dependancies to local variables
    var _       = global._;
    var $       = global.jQuery;
    var window  = global.window;


    // Constructor
    // ===========

    var Trapeze = (global.Trapeze || (global.Trapeze = { }));
    var AlmayCoupon = Trapeze.AlmayCoupon = Trapeze.AlmayCoupon || { };

    var Core = Trapeze.Core = function (options) {

        this.config         = $.extend(true, defaults, options || { });

        this._initialize();
    };


    // Initialization
    // ==============

    Core.prototype._initialize = function () {
        this._initializePage();
    };

    Core.prototype._initializePage = function () {
        var classnames  = $('body').prop('class').split(' ');
        var index       = classnames.length;

        this._initializeFacebook();

        if (index > 1) {
            while(index--) {
                switch (classnames[index]) {
                    case 'home-page':
                        new Analytics.Form();
                        break;
                    case 'detail-page':
                        // Execute page specific javascript
                        break;

                    case 'form-page':
                        break;

                    case 'contact-page':
                        break;
                }
            }
        }
    };

    Core.prototype._initializeFacebook = function () {
        Trapeze.IFrame.initialize();

        AlmayCoupon.Facebook.on('initialized', _(function (facebookCanvasAPI) {
            if(!Trapeze.IFrame.isIFrame()) {
                facebookCanvasAPI.scrollTo = Core._pseudoScrollTo;
            }
            this.attachSocialClicks(_(facebookCanvasAPI.post).bind(facebookCanvasAPI));
        }).bind(this));
    };

}).call(this);
