(function () {
    var global = this;


    var appDependencies = function (global, factory) {
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define(['underscore'], function (_) {
                // Also create a global in case some scripts
                // that are loaded still are looking for
                // a global even when an AMD loader is in use.
                return (global.Analytics = factory(_));
            });
        } else {
            // Browser globals
            global.Analytics = factory(global._);
        }
    };


    var appFactory = function(_) {

        var NAME    = 'Analytics';

        // Debugging
        var Debug = global.Debug;
        var debug = !!(Debug && Debug.constructor && Debug.call && Debug.apply) ? new Debug(NAME) : { };
        var log   = debug.bind ? debug.bind('log') : function () { };

        // Empty constuctor
        var ctor = function () { };


        // Variables
        var Analytics = { };


        // EventType Class
        // ===============

        var EventType = Analytics.EventType = function () {
            this._initialize();
        };

        EventType.prototype._initialize = function () {
            this.callbacks = [ ];
        };

        EventType.prototype.addCallback = function (callback) {
            this.callbacks.push(callback);
        };

        EventType.prototype.track = function () {
            var args = Array.prototype.slice.apply(arguments);
            this._triggerCallbacks(args);
        };

        EventType.prototype.getTrackWithContext = function () {
            return _(this.track).bind(this);
        };

        EventType.prototype._triggerCallbacks = function (args) {
            var length = this.callbacks.length;
            var index, callback;

            var self = new ctor();
            ctor.prototype = null;
            for (index = 0; index < length; index++) {
                callback = this.callbacks[index];

                if (_.isFunction(callback)) {
                    callback.apply(self, args);
                }
            }
        };


        // Events
        // ======

        Analytics.event     = new EventType();
        Analytics.social    = new EventType();


        return Analytics;

    };


    // Pass dependencies to factory
    appDependencies(global, appFactory);

}).call(this);
