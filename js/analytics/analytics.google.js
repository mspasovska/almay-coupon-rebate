(function () {
    var global = this;


    var appDependencies = function (global, factory) {
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define('js/analytics.google.js',['jquery', 'underscore', 'analytics'], function ($, _) {
                // Also create a global in case some scripts
                // that are loaded still are looking for
                // a global even when an AMD loader is in use.
                return (global.Analytics.Google = factory($, _, Analytics));
            });
        } else {
            // Browser globals
            global.Analytics.Google = factory(global.$, global._, global.Analytics);
        }
    };


    var appFactory = function($, _, Analytics) {

        var NAME    = 'Analytics -- Google';

        // Debugging
        var Debug = global.Debug;
        var debug = !!(Debug && Debug.constructor && Debug.call && Debug.apply) ? new Debug(NAME) : { };
        var log   = debug.bind ? debug.bind('log') : function () { };
        var error = debug.bind ? debug.bind('error') : function () { };


        // Constants
        var NAMESPACE   = 'ga';


        // Google Analytics
        // ===================================================

        var Google = function (options) {
            var defaults = {
                account     : null,
                pageview    : null,
                namespace   : null,
                parameters  : [ ]
            };

            this.config = $.extend(true, defaults, options || { });

            // Initialize modal
            this._initialize();
        };


        // Initialization
        // ==============

        Google.prototype._initialize = function () {
            var scope = this.config.scope || 'body';

            this._configureSDK();
        };


        // Load SDK
        // ========

        Google.prototype._configureSDK = function () {
            if (!_.isString(this.config.account)) {
                return;
            }

            var parameters  = this.config.parameters;

            var gaq         = global[NAMESPACE] = global[NAMESPACE] || [];
            var length, index;

            _gaq.push(['_setAccount', this.config.account]);

            if (_.isString(this.config.pageview)) {
                _gaq.push(['_trackPageview', this.config.pageview]);
            } else {
                _gaq.push(['_trackPageview']);
            }

            if (_.isArray(parameters)) {
                length = parameters.length;

                for (index = 0; index < length; index++) {
                    _gaq.push(parameters[index]);
                }
            }

            this._loadSDK();
        };

        Google.prototype._loadSDK = function () {
            var js_first    = document.getElementsByTagName('script')[0];
            var js_ga       = document.createElement('script');

            js_ga.type     = 'text/javascript';
            js_ga.async    = true;
            js_ga.src      = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            js_first.parentNode.insertBefore(js_ga, js_first);
        };


        // Global event / social functions
        // ===================================================

        Analytics.pushGoogleEvent = function(category, tag, label) {
            if (category && tag && category.length > 0 && tag.length > 0) {
                Analytics.pushGoogleAnalytics(['send','event', category, tag, label]);
            } else {
                error('_trackEvent missing data, push failed.', arguments);
            }
        };

        Analytics.event.addCallback(Analytics.pushGoogleEvent);


        Analytics.pushGoogleSocial = function(network, action, target) {
            if (network && action && network.length > 0 && action.length > 0) {
                Analytics.pushGoogleAnalytics(['_trackSocial', network, action, target]);
            } else {
                error('_trackSocial missing data, push failed.', arguments);
            }
        };

        Analytics.social.addCallback(Analytics.pushGoogleSocial);


        // Global push function
        // ===================================================

        Analytics.pushGoogleAnalytics = function(args) {
            var _gaq        = global[NAMESPACE];

            if (ga) {
               ga(args[0], args[1], args[2], args[3], args[4]);
                log('_gaq.push', args);
            } else {
                error('_gaq not found, push failed.');
            }
        };


        return Google;

    };


    // Pass dependencies to factory
    appDependencies(global, appFactory);

}).call(this);
