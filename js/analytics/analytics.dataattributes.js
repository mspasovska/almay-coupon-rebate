(function () {
    var global = this;


    var appDependencies = function (global, factory) {
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define('js/analytics.dataattributes.js',['jquery', 'underscore', 'analytics'], function ($, _) {
                // Also create a global in case some scripts
                // that are loaded still are looking for
                // a global even when an AMD loader is in use.
                return (global.Analytics.DataAttributes = factory($, _, Analytics));
            });
        } else {
            // Browser globals
            global.Analytics.DataAttributes = factory(global.$, global._, global.Analytics);
        }
    };


    var appFactory = function($, _, Analytics) {

        var NAME    = 'Analytics -- DataAttributes';

        // Debugging
        var Debug = global.Debug;
        var debug = !!(Debug && Debug.constructor && Debug.call && Debug.apply) ? new Debug(NAME) : { };
        var log   = debug.bind ? debug.bind('log') : function () { };


        // Constants
        var NAMESPACE   = '_gaq';


        // Data Attributes
        // ===================================================

        var DataAttributes = function (options) {
            var defaults = {
                scope       : null,

                event : {
                    category    : 'data-event-category',
                    tag         : 'data-event-tag',
                    label       : 'data-event-label',

                    callback    : null
                },

                social : {
                    network     : 'data-social-network',
                    action      : 'data-social-action',
                    target      : 'data-social-target',

                    callback    : null
                },

                namespace   : 'analytics-data-attributes'
            };

            this.config = $.extend(true, defaults, options || { });

            // Initialize modal
            this._initialize();
        };


        // Initialization
        // ==============

        DataAttributes.prototype._initialize = function () {
            var scope = this.config.scope || 'body';

            this._attachEventClick(scope);
            this._attachSocialClick(scope);
        };


        // Events
        // ======

        DataAttributes.prototype._attachEventClick = function(scope) {
            var selector    = '[' + this.config.event.category + ']';
            var event       = 'click.' + this.config.namespace;
            var handler     = _(this._handleEventClick).bind(this);

            $(scope)
                .find(selector)
                .off(event)
                .on(event, handler);
        };

        DataAttributes.prototype._handleEventClick = function(event) {
            var $target     = $(event.currentTarget);
            var category    = $target.attr(this.config.event.category);
            var tag         = $target.attr(this.config.event.tag);
            var label       = $target.attr(this.config.event.label);

            var callback    = this.config.event.callback;

            if (!_.isFunction(callback)) {
                callback = Analytics.event.getTrackWithContext();
            }

            callback(category, tag, label);
        };

        DataAttributes.prototype._attachSocialClick = function(scope) {
            var selector    = '[' + this.config.social.network + ']';
            var event       = 'click.' + this.config.namespace;
            var handler     = _(this._handleSocialClick).bind(this);

            $(scope)
                .find(selector)
                .off(event)
                .on(event, handler);
        };

        DataAttributes.prototype._handleSocialClick = function(event) {
            var $target     = $(event.currentTarget);
            var network     = $target.attr(this.config.social.network);
            var action      = $target.attr(this.config.social.action);
            var target      = $target.attr(this.config.social.target);

            var callback    = this.config.social.callback;

            if (!_.isFunction(callback)) {
                callback = Analytics.social.getTrackWithContext();
            }

            callback(network, action, target);
        };


        // Execute on DOM ready
        // ====================

        $(function () {
            new DataAttributes();
        });


        return DataAttributes;

    };


    // Pass dependencies to factory
    appDependencies(global, appFactory);

}).call(this);
